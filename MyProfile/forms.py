from django import forms
from .models import Agenda

class CreateAgendaForm(forms.Form):
    date_attrs = {
        'type':'date',
        'class': 'form-control',
    }

    hour_attrs = {
        'type':'time',
        'class': 'form-control',
    }

    activity_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Nama kegiatan...'
    }

    location_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Nama tempat...'
    }

    category_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Nama kategori....'
    }

    date = forms.DateTimeField(label='Date', required=True, localize=True, widget=forms.DateInput(attrs=date_attrs))
    hour = forms.TimeField(label='Hour', required=True, localize=True, widget=forms.TimeInput(attrs=hour_attrs))
    activity = forms.CharField(label='Activity', max_length=60,  required=True, widget=forms.TextInput(attrs=activity_attrs))
    location = forms.CharField(label='Location', max_length=60, required=True, widget=forms.TextInput(attrs=location_attrs))
    category = forms.CharField(label='Category', max_length=60, required=True, widget=forms.TextInput(attrs=category_attrs))

class CustomModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{}: {}, {}".format(obj.activity, obj.day, obj.date)

class DeleteAgendaForm(forms.Form):
    choice_attrs = {
        'class':'custom-select'
    }

    agendas = Agenda.objects.all()
    choice = CustomModelChoiceField(label="Activity", required=True, widget=forms.Select(choice_attrs), queryset=agendas, to_field_name="activity")

