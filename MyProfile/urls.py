from django.urls import path
from . import views

app_name = 'MyProfile'

urlpatterns = [
    path('', views.index, name='index'),
    path('moreaboutme/', views.moreaboutme, name='moreaboutme'),
    path('skill/', views.skill, name='skill'),
    path('agenda_input/', views.agenda_input, name='agenda-input'),
    path('agenda_view/', views.agenda_view, name='agenda-view'),
    path('agenda_delete/<id>', views.delete_agenda, name="agenda-delete")
]
