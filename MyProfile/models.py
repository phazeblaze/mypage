from django.db import models
from datetime import date
# Create your models here.

class Agenda(models.Model):
    date = models.DateField()
    day = models.CharField(max_length=60)
    hour = models.TimeField()
    activity = models.CharField(max_length=60)
    location = models.CharField(max_length=60)
    category = models.CharField(max_length=60)