from django.shortcuts import render, redirect
from .forms import CreateAgendaForm, DeleteAgendaForm
from .models import Agenda
# Create your views here.

def index(request):
    return render(request, 'index.html')

def moreaboutme(request):
    return render(request, 'moreaboutme.html')

def skill(request):
    return render(request, 'skill.html')

def agenda_input(request):
    form = CreateAgendaForm()

    if(request.method == 'POST'):
        form = CreateAgendaForm(request.POST)
        if (form.is_valid()):
            date = form.cleaned_data['date']
            day = date.strftime('%A')
            hour = form.cleaned_data['hour']
            activity = form.cleaned_data['activity']
            location = form.cleaned_data['location']
            category = form.cleaned_data['category']
            

            agenda = Agenda(
                date = date,
                day = day,
                hour = hour,
                activity = activity,
                location = location,
                category = category
            )

            agenda.save()
            return redirect("MyProfile:agenda-view")

    response = {'create_agenda_form': form} #'delete_agenda_form': DeleteAgendaForm}
    
    return render(request, 'agenda-input.html', response)

def save_jadwal(request):
    response = {'save_status':''}
    if(request.method == 'POST'):
        form = CreateAgendaForm(request.POST)
        if (form.is_valid()):
            date = form.cleaned_data['date']
            day = date.strftime('%A')
            hour = form.cleaned_data['hour']
            activity = form.cleaned_data['activity']
            location = form.cleaned_data['location']
            category = form.cleaned_data['category']
            

            agenda = Agenda(
                date = date,
                day = day,
                hour = hour,
                activity = activity,
                location = location,
                category = category
            )

            agenda.save()

            response['save_status'] = 'ooh yeah ooh yea'
        else:
            response['save_status'] = 'thats a bruh moment'
    else:
        response['save_status'] = 'uwu'

    return render(request, 'saved.html', response)

def delete_agenda(request, id):
    # response = {'delete_status':''}
    # if(request.method == 'POST'):
    #     form = DeleteAgendaForm(request.POST)
    #     if (form.is_valid()):
    #         agenda = form.cleaned_data['choice']
    #         agenda.delete()

    #         response['delete_status'] = 'snap'
    #     else:
    #         response['delete_status'] = 'sike'
    # else:
    #     response['delete_status'] = 'uwu, idk man idk why it happened like this im sorry'

    # return render(request, 'deleted.html', response)
    deleted_agenda = Agenda.objects.get(id=id)
    deleted_agenda.delete()
    return redirect("MyProfile:agenda-view")

def agenda_view(request):
    agendas = Agenda.objects.all()

    response = {'agendas':agendas}
    return render(request, 'agenda-view.html', response)



#def agenda_input(request):
#    return render(request, 'agenda-input.html')

#def agenda_view(request):
#    return render(request, 'agenda-view.html')
